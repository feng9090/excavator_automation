#!/usr/bin/env python
# coding:utf-8

import rospy
from std_msgs.msg import String

def command_pub():

    pub = rospy.Publisher('command', String, queue_size=10)
    rospy.init_node('command_pub')
    rate = rospy.Rate(10) # 10hz

    while not rospy.is_shutdown():
        
        msg_to_send = "DIG"
        pub.publish(msg_to_send)
        rate.sleep()

if __name__ == '__main__':
    try:
        command_pub()
    except rospy.ROSInterruptException:
        pass