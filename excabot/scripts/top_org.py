#!/usr/bin/env python
# coding:utf-8

######################################################################################
##                                  top_organizer                                   ##
## subscribe topic: top_command (msg:String (command to robot top action)           ##
## publish   topic: boom_error/arm_error/bucket_error/swing_error (msg: Float32)    ##
## function: convert action into specific error before controllers                  ##
######################################################################################

import rospy 
from excabot.msg import top_angle
from std_msgs.msg import String
from std_msgs.msg import Float32
import time

command = "WAIT" # "WAIT": wait for new command, "DIG": operate digging action

boom_pos = arm_pos = bucket_pos = swing_pos = 0.0

def command_callback(data):
    global command
    
    if command == "WAIT":
        command = data.data
        # print(command)

def sensor_callback(data):
    global boom_pos, arm_pos, bucket_pos, swing_pos

    boom_pos = data.boom_angle
    arm_pos = data.arm_angle
    bucket_pos = data.bucket_angle
    swing_pos = data.swing_angle
    # print(boom_pos, arm_pos, bucket_pos, swing_pos)

def sending_error(boom_ref, arm_ref, bucket_ref, swing_ref, boom_threshold, arm_threshold, bucket_threshold, swing_threshold):
    global boom_pos, arm_pos, bucket_pos, swing_pos

    boom_error_pub = rospy.Publisher('boom_error', Float32, queue_size = 10)
    arm_error_pub = rospy.Publisher('arm_error', Float32, queue_size = 10)
    bucket_error_pub = rospy.Publisher('bucket_error', Float32, queue_size = 10)
    swing_error_pub = rospy.Publisher('swing_error', Float32, queue_size = 10)

    work_state = "Not ready" 
    # "Not ready": keep sending error to control node
    # "ready": stop current loop and continue next action

    while work_state == "Not ready":
        # print(work_state)

        # send boom_error to boom_control
        boom_error = boom_ref - boom_pos
        arm_error = arm_ref - arm_pos
        bucket_error = bucket_ref - bucket_pos
        swing_error = swing_ref - swing_pos
        no_error = 0.0

        # print(1)
        # print(boom_ref, arm_ref, bucket_ref, swing_ref)
        # print(2)
        # print(boom_pos, arm_pos, bucket_pos, swing_pos)
        # print(3)
        # print(boom_error, arm_error, bucket_error, swing_error)
        # time.sleep(1)

        all_set = (abs(boom_error) < boom_threshold) & (abs(arm_error) < arm_threshold) & (abs(bucket_error) < bucket_threshold) & (abs(swing_error) < swing_threshold)
        
        if (all_set):
            work_state = "Ready"
            boom_error_pub.publish(no_error)
            arm_error_pub.publish(no_error)
            bucket_error_pub.publish(no_error)
            swing_error_pub.publish(no_error)
            time.sleep(0.5)

        else:
            boom_error_pub.publish(boom_error)
            arm_error_pub.publish(arm_error)
            bucket_error_pub.publish(bucket_error)
            swing_error_pub.publish(swing_error)

def dig():
    global command

    dig_state = "Initialization" 
    # "Initialization": pulling up to ideal position,
    # "Locating spot": pulling down to digging position, 
    # "Collecting": take back arm and bucket,
    # "Pulling up": taking up bucket and ready to load,
    # "Done": finishing job

    while True:
        # print(dig_state)

        if dig_state == "Done":
            # print(dig_state)
            command = "WAIT"
            return

        elif dig_state == "Initialization":

            # print(dig_state)
            boom_ref = 45; arm_ref = -45; bucket_ref = 0; swing_ref = 0
            boom_threshold = 5; arm_threshold = 5; bucket_threshold = 5; swing_threshold = 5

            sending_error(boom_ref, arm_ref, bucket_ref, swing_ref, boom_threshold, arm_threshold, bucket_threshold, swing_threshold)
            dig_state = "Locating spot"
            # print(dig_state)

        elif dig_state == "Locating spot":
            
            # print(dig_state)
            boom_ref = 0; arm_ref = -45; bucket_ref = 0; swing_ref = 0
            boom_threshold = 5; arm_threshold = 5; bucket_threshold = 5; swing_threshold = 5

            sending_error(boom_ref, arm_ref, bucket_ref, swing_ref, boom_threshold, arm_threshold, bucket_threshold, swing_threshold)
            dig_state = "Collecting"
            # print(dig_state)

        elif dig_state == "Collecting":

            process_level = 9

            boom_ref = 0; arm_ref = -45; bucket_ref = 0; swing_ref = 0
            boom_end = 15; arm_end = -90; bucket_end = -90; swing_end = 0

            boom_step = (boom_end - boom_ref) / process_level
            arm_step = (arm_end - arm_ref) / process_level
            bucket_step = (bucket_end - bucket_ref) / process_level
            swing_step = (swing_end - swing_ref) / process_level

            boom_threshold = 3; arm_threshold = 3; bucket_threshold = 3; swing_threshold = 3
            
            process_lev = 0

            while process_level < 9:

                boom_ref += boom_step
                arm_ref += arm_step
                bucket_ref += bucket_ref
                swing_ref += swing_step

                sending_error(boom_ref, arm_ref, bucket_ref, swing_ref, boom_threshold, arm_threshold, bucket_threshold, swing_threshold)
        
            dig_state = "Pulling up"

        elif dig_state == "Pulling up":

            boom_ref = 45; arm_ref = -135; bucket_ref = -90; swing_ref = 0
            boom_threshold = 5; arm_threshold = 5; bucket_threshold = 5; swing_threshold = 5

            sending_error(boom_ref, arm_ref, bucket_ref, swing_ref, boom_threshold, arm_threshold, bucket_threshold, swing_threshold)
            
            dig_state = "Done"


def top_organizer():

    rospy.init_node('top_organizer')

    rospy.Subscriber("top_position", top_angle, sensor_callback)
    rospy.Subscriber("command", String, command_callback)

    while not rospy.is_shutdown():
        
        if command == "WAIT":
            # print(command)
            pass

        elif command == "DIG":
            # print(command)
            dig()

if __name__ == '__main__':
    
    try:      
        top_organizer()
    except rospy.ROSInterruptException:
        pass