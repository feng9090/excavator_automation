#!/usr/bin/env python
# coding:utf-8

##############################################################################
##                              boom_controller                             ##
## subscribe topic: boom_error (msg: Float32)                               ##
## publish   topic: boom_pwm (msg: Int16)                                   ##
## function       : calculating the boom pwm according to error             ##
##############################################################################

import rospy
from std_msgs.msg import Float32
from std_msgs.msg import Int16

# proportional gain setting up
kp = 1.0

# boom error: subscribe error from "boom_error"

# movement direction: indicate the direction with positive or negative marker

def error_callback(data):
    
    error = data.data
    boom_pwm = max(int(kp * error), 100)

    pub = rospy.Publisher('boom_pwm', Int16, queue_size = 10)
    
    pub.publish(boom_pwm)

def boom_control():
    rospy.init_node('boom_controller')

    rospy.Subscriber("boom_error", Float32, error_callback)

if __name__ == '__main__':
    try:
        boom_control()
    except rospy.ROSInterruptException:
        pass
