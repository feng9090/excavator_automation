#!/usr/bin/env python
# coding:utf-8

##############################################################################
##                               bucket_controller                          ##
## subscribe topic: bucket_error (msg: Float32)                             ##
## publish   topic: bucket_pwm (msg: Int16)                                 ##
## function       : calculating the bucket pwm according to error           ##
##############################################################################

import rospy
from std_msgs.msg import Float32
from std_msgs.msg import Int16

# proportional gain setting up
kp = 1.0

# bucket error: subscribe error from "bucket_error"

# movement direction: indicate the direction with positive or negative marker

def error_callback(data):
    
    error = data.data
    bucket_pwm = max(int(kp * error), 100)

    pub = rospy.Publisher('bucket_pwm', Int16, queue_size = 10)
    
    pub.publish(bucket_pwm)

def bucket_control():
    rospy.init_node('bucket_controller')

    rospy.Subscriber("bucket_error", Float32, error_callback)

if __name__ == '__main__':
    try:
        bucket_control()
    except rospy.ROSInterruptException:
        pass