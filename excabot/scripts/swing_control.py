#!/usr/bin/env python
# coding:utf-8

##############################################################################
##                                swing_controller                          ##
## subscribe topic: swing_error (msg: Float32)                              ##
## publish   topic: swing_pwm (msg: Int16)                                  ##
## function       : calculating the swing pwm according to error            ##
##############################################################################

import rospy
from std_msgs.msg import Float32
from std_msgs.msg import Int16

# proportional gain setting up
kp = 1.0

# swing error: subscribe error from "swing_error"

# movement direction: indicate the direction with positive or negative marker

def error_callback(data):
    
    error = data.data
    swing_pwm = max(int(kp * error), 100)

    pub = rospy.Publisher('swing_pwm', Int16, queue_size = 10)
    
    pub.publish(swing_pwm)

def swing_control():
    rospy.init_node('swing_controller')

    rospy.Subscriber("swing_error", Float32, error_callback)

if __name__ == '__main__':
    try:
        swing_control()
    except rospy.ROSInterruptException:
        pass