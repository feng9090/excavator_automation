#!/usr/bin/env python
# coding:utf-8

##############################################################################
##                               arm_controller                             ##
## subscribe topic: arm_error (msg: Float32)                                ##
## publish   topic: arm_pwm (msg: Int16)                                    ##
## function       : calculating the arm pwm according to error              ##
##############################################################################

import rospy
from std_msgs.msg import Float32
from std_msgs.msg import Int16

# proportional gain setting up
kp = 1.0

# arm error: subscribe error from "arm_error"

# movement direction: indicate the direction with positive or negative marker

def error_callback(data):
    
    error = data.data
    arm_pwm = max(int(kp * error), 100)

    pub = rospy.Publisher('arm_pwm', Int16, queue_size = 10)
    
    pub.publish(arm_pwm)

def arm_control():
    rospy.init_node('arm_controller')

    rospy.Subscriber("arm_error", Float32, error_callback)

if __name__ == '__main__':
    try:
        arm_control()
    except rospy.ROSInterruptException:
        pass
