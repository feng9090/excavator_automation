#!/usr/bin/env python
# coding:utf-8

import rospy
from excabot.msg import top_angle

def sensor_pub():

    pub = rospy.Publisher('top_position', top_angle, queue_size=10)
    rospy.init_node('sensor_pub')
    rate = rospy.Rate(1) # 10hz

    while not rospy.is_shutdown():
        
        msg_to_send = top_angle()

        msg_to_send.boom_angle = 45
        msg_to_send.arm_angle = -45
        msg_to_send.bucket_angle = 0.1 
        msg_to_send.swing_angle = 0.1
        print(msg_to_send)

        pub.publish(msg_to_send)
        rate.sleep()

if __name__ == '__main__':
    try:
        sensor_pub()
    except rospy.ROSInterruptException:
        pass